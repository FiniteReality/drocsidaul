local copas = require("copas")
local drocsidaul = require("drocsidaul.base")
local http = require("drocsidaul.http")
local json = require("cjson")
local protocol = require("drocsidaul.protocol")
local util = require("drocsidaul.util")
local websocket_client = require("websocket.client_copas")
local websocket_frame = require("websocket.frame")
local zlib = require("zlib")

local client = { }
local client_mt = { }
local client_methods = { }
client_mt.__index = client_methods

client.defaults = {
    gateway_version = "6",
    gateway_encoding = "json",
    gateway_compress = "zlib-stream",
    websocket = {
        timeout = 0,
        exponential_backoff = 1.75,
        ssl = {
            mode = "client",
            protocol = "tlsv1_2"
        }
    },
    large_threshold = 250
}

function set_state(client, state)
    if client.state ~= state then
        client.state = state
        client:fire_event("connection_state", state)
    end
end

function client_methods:fire_event(name, ...)
    for i, v in ipairs(self.__events[name]) do
        v(...)
    end
end

function client_methods:on(event, handler)
    local event_t = assert(self.__events[event], "unknown event " .. event)
    event_t[#event_t + 1] = handler
end

function client_methods:disconnect(code, reason)
    self:fire_event("log", ("disconnecting: %s")
        :format(reason or "disconnect called"))

    self.__websocket:close(code or 1000, reason or "client disconnected")
    self.__cache.decompressor = nil

    set_state(self, "disconnected")
end

function client_methods:connect(shard_count, shard_id)
    assert(self.state == "disconnected", "client must be disconnected")

    -- update the cached shard info if specified for when we identify
    if shard_id or shard_count then
        assert(shard_id and shard_count,
            "both shard id and count must be specified")
        self.__cache.shard_info = {shard_id, shard_count}
    end

    self:fire_event("log", "connecting to gateway")

    local attempts = 0
    while true do
        local gateway_url = self.__cache.gateway_url

        -- attempt to connect to our pre-cached gateway url
        if gateway_url then
            set_state(self, "connecting")

            attempts = attempts + 1
            self:fire_event("log", ("connecting to %s"):format(gateway_url))

            -- luasec overwrites our timeout so we need to forcibly set it on
            -- the first receive
            local hasSetTimeout = false
            self.__websocket.sock_receive = function(ws, ...)
                if not hasSetTimeout then
                    hasSetTimeout = true
                    ws.sock:settimeout(self.__options.websocket.timeout)
                end

                return copas.receive(ws.sock, ...)
            end

            local succ, err = self.__websocket:connect(gateway_url, nil,
                self.__options.websocket.ssl)

            if not succ then
                self:fire_event("log", ("error connecting: %s"):format(err))

                set_state(self, "disconnected")

                -- reset our url if we fail to connect to it too many times
                if attempts > 5 then
                    gateway_url = nil
                end

                copas.sleep(
                    self.__options.websocket.exponential_backoff ^ attempts)
            else
                self:fire_event("log", "connected websocket")
                set_state(self, "connected")
                self.__cache.decompressor = zlib.inflate()

                return
            end
        end

        -- if we don't have a gateway url, or we failed to connect to it, get
        -- another
        if not gateway_url then
            attempts = 0
            self:fire_event("log", "retrieving gateway url")
            self.__cache.gateway_url = self.__api:get_gateway_url(
                self.__options.gateway_version,
                self.__options.gateway_encoding,
                self.__options.gateway_compress)
        end
    end
end

function client_methods:think()
    if self.state ~= "connected" then
        return false
    end

    local data, opcode, clean, code, reason = self.__websocket:receive()

    if data then
        if opcode == websocket_frame.TEXT then
            -- deserialize pure json frames
            data = json.decode(data)
        elseif opcode == websocket_frame.BINARY then
            -- decompress and deserialize zlib-compressed frames
            data = self.__cache.decompressor(data)
            data = json.decode(data)
        end

        protocol.handle_frame(self, data)
        self:fire_event("data", data)

        return true
    else
        self:fire_event("log", ("connection close (%s, %s): %s")
            :format(clean and "clean" or "unclean", code, reason))

        set_state(self, "disconnected")

        self.__cache.decompressor = nil
        copas.wakeup(self.__cache.heartbeat_thread)
        self.__cache.heartbeat_thread = nil

        return false
    end
end

function client_methods:send_opcode(opcode, data, dispatch_type)
    assert(self.state == "connected", "client must be connected")

    self:fire_event("log", ("sending opcode %d"):format(opcode))

    data = {op = opcode, d = data, t = dispatch_type}
    data = json.encode(data)

    return self.__websocket:send(data, websocket_frame.TEXT)
end

function client.new(token, options)
    options = util.merge(options, client.defaults)

    local o = {
        state = "disconnected",
        __token = token,
        __websocket = websocket_client(options.websocket),
        __api = http.new(token, options.http),
        __events = {
            connection_state = { },
            data = { },
            dispatch = { },
            latency = { },
            log = { },
        },
        __options = options,
        __cache = {
            gateway_url = options.gateway,
            sequence = 0,
            heartbeat_received = 0,
            heartbeat_sent = 0
        }
    }

    o.__websocket.sock_close = function(self)
        return self.sock:close()
    end

    return setmetatable(o, client_mt)
end

return client