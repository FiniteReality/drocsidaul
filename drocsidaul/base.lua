return {
    _NAME = "Drocsidaul",
    _VERSION = "1.0.0",
    _URL = "https://gitlab.com/FiniteReality/drocsidaul",
    _COPYRIGHT = "Copyright (C) 2018+ FiniteReality",
    _DESCRIPTION = "A Copas based Discord bot library"
}