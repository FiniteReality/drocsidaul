local bucket = require("drocsidaul.bucket")
local drocsidaul = require("drocsidaul.base")
local json = require("cjson")
local ltn12 = require("ltn12")
local platform = require("drocsidaul.platform")
local rawHttp = require("copas.http")
local url = require("socket.url")
local util = require("drocsidaul.util")
local uuid = require("uuid")
local zlib = require("zlib")

-- ensure the UUID generator is seeded, so we get good quality UUIDs
uuid.seed()

local http = { }
local http_mt = { }
local http_methods = { }
http_mt.__index = http_methods

local function get_user_agent()
    local agent = ("DiscordBot (%s, %s)")
        :format(drocsidaul._URL, drocsidaul._VERSION)

    if platform.lua_version then
        agent = ("%s %s"):format(agent, platform.lua_version)
    end

    if platform.luajit_version then
        agent = ("%s %s"):format(agent, platform.luajit_version)
    end

    return agent
end

http.defaults = {
    base_url = "https://discordapp.com/api",
    api_version = "6",
    user_agent = get_user_agent()
}

local format_route do
    local major_params = {
        ["channel_id"] = true,
        ["guild_id"] = true,
        ["webhook_id"] = true
    }
    function format_route(self, route, query)
        local url = route
        local bucket_url = route

        for param in route:gmatch("%b{}") do
            local param_name = param:match("^{(.+)}$")

            url = url:gsub(param, query[param_name])
            if major_params[param_name] then
                bucket_url = bucket_url:gsub(param, query[param_name])
            end
        end

        local bucketInfo = self.__ratelimits[bucket_url]
        if not bucketInfo then
            bucketInfo = bucket.new()
            self.__ratelimits[bucket_url] = bucketInfo
        end

        url = ("%s/v%d/%s"):format(self.__options.base_url,
            self.__options.api_version, url)

        return url, bucketInfo
    end
end

local function build_query_string(params)
    local query = { }

    for i, v in pairs(params) do
        query[#query+1] = string.format("%s=%s", url.escape(i), url.escape(v))
    end

    return table.concat(query, '&')
end

function http_methods:request(method, route, params)
    local url, bucketInfo = format_route(self, route, params)

    bucketInfo:enter()

    local req_body = nil
    local content_type = nil

    if params then
        if params._body_generator then
            req_body = params._body_generator
        elseif params._body then
            req_body = ltn12.source.string(params._body)
        end
        if req_body then
            content_type = params._content_type or "application/json"
        end
    end

    local resp_body = { }
    local success, code, headers = rawHttp.request{
        method = method,
        url = url,
        sink = ltn12.sink.table(resp_body),
        source = req_body,
        headers = {
            ["Transfer-Encoding"] = req_body and "chunked",
            ["Content-Type"] = content_type,
            Authorization = ("Bot %s"):format(self.__token)
        }
    }

    assert(success, "http request returned unsuccessfully ("..code..")")

    bucketInfo:update(headers)

    assert(code == 200 or code == 204,
        ("API error occured (%s)"):format(code))

    local body = table.concat(resp_body)
    return json.decode(body)
end

function http_methods:get_gateway_url(version, encoding, compress)
    local url = self:request("GET", "gateway").url

    local query = build_query_string{
        v = version,
        encoding = encoding,
        compress = compress
    }

    return ("%s/?%s"):format(url, query)
end

local multipart_preamble_json = table.concat({
    "Content-Disposition: form-data; name=\"payload_json\";",
    "Content-Type: application/json",
    "", ""
}, "\r\n")

local multipart_preamble_file = table.concat({
    "Content-Disposition: form-data; name=\"file%s\"; filename=%q",
    "Content-Type: application/octet-stream",
    "", ""
}, "\r\n")

function http_methods:create_message(channel_id, content, options)
    local payload = {
        content = content,
        nonce = options and options.nonce,
        tts = options and options.tts,
        embed = options and options.embed
    }

    local default_body = ltn12.source.string(json.encode(payload))
    local request_options = {
        channel_id = channel_id,
        _body_generator = default_body
    }

    if options then
        --assert(not options.files, "file upload not yet implemented")
        local boundary = ("-----%s-----"):format(uuid.new())

        request_options._content_type = ("multipart/form-data; boundary=%s")
            :format(boundary)

        local contents = {}

        contents[#contents+1] = ltn12.source.string(
            ("--%s\r\n"):format(boundary))
        contents[#contents+1] = ltn12.source.string(multipart_preamble_json)
        contents[#contents+1] = default_body

        for i, file in ipairs(options.files) do
            contents[#contents+1] = ltn12.source.string(
                ("\r\n--%s\r\n"):format(boundary))

            if type(file) == "string" then
                -- path to file
                local filename = file:match("[\\/]?(.-)$")
                contents[#contents+1] = ltn12.source.string(
                    multipart_preamble_file:format(i, filename))
                contents[#contents+1] = ltn12.source.file(io.open(file, "rb"))

            elseif type(file) == "userdata" then
                -- pre-opened file or file-like object
                contents[#contents+1] = ltn12.source.string(
                    multipart_preamble_file:format(i, "upload"))
                contents[#contents+1] = ltn12.source.file(file)

            elseif type(file) == "table" then
                -- table contents
                contents[#contents+1] = ltn12.source.string(
                    multipart_preamble_file:format(i, file.name or "upload"))

                local pos = 0
                contents[#contents+1] = function()
                    pos = pos + 1
                    local value = file[pos]
                    if value then
                        assert(type(value) == "string",
                            "table item was not a string")
                    end
                    return value
                end
            end

            if i == #options.files then
                contents[#contents+1] = ltn12.source.string(
                    ("\r\n--%s--\r\n"):format(boundary))
            else
                -- no terminator here as we have more to process
                -- terminator is added there
                contents[#contents+1] = ltn12.source.string(
                    ("\r\n--%s"):format(boundary))
            end
        end

        request_options._body_generator = ltn12.source.cat(unpack(contents))
    end

    return self:request(
        "POST", "channels/{channel_id}/messages", request_options)
end

function http.new(token, options)
    options = util.merge(options, http.defaults)
    local o = {
        __token = token,
        __options = options,
        __ratelimits = { }
    }
    return setmetatable(o, http_mt)
end

return http