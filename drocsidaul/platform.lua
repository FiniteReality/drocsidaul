local drocsidaul = require("drocsidaul.base")
local isLuaJit, jit = pcall(require, "jit")

return {
    gateway_properties = {
        ["os"] = isLuaJit and jit.os,
        ["$browser"] = drocsidaul._NAME,
        ["$device"] = drocsidaul._NAME
    },
    lua_version = _VERSION,
    luajit_version = isLuaJit and jit.version
}