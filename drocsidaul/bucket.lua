local copas = require("copas")
local date = require("date")

local bucket = { }
local bucket_mt = { }
local bucket_methods = { }
bucket_mt.__index = bucket_methods

function bucket_methods:enter()
    if self.__remaining == 0 then
        local co = coroutine.running()
        self.__sleeping[#self.__sleeping + 1] = co
        copas.sleep(-1)
    end

    self.__remaining = self.__remaining - 1
end

function bucket_methods:update(headers)
    self.__offset = date.diff(date(true), date(headers["date"]))

    self.__limit = tonumber(headers["x-ratelimit-limit"]) or 5
    self.__remaining = tonumber(headers["x-ratelimit-remaining"]) or 5
    self.__reset = date(tonumber(headers["x-ratelimit-reset"]) or true)

    if not tonumber(headers["x-ratelimit-reset"]) then
        -- reset in 1 minute to be safe
        self.__reset:addseconds(60)
    end

    copas.wakeup(self.__timer)
end

function bucket.new()
    local o = {
        __offset = nil,
        __reset = nil,
        __limit = 5,
        __remaining = 5,
        __sleeping = { }
    }

    o.__timer = copas.addthread(function(self)
        copas.sleep(-1)
        while true do
            local serverNow = date(true) + self.__offset

            if serverNow > self.__reset then
                self.__remaining = self.__limit

                local i = #self.__sleeping
                while i > 0 and self.__remaining > 0 do
                    copas.wakeup(self.__sleeping[i])
                    self.__sleeping[i] = nil
                    i = i - 1
                end
            end

            -- if serverNow is in the future, we will sleep until woken
            -- which is intentional
            local timeout = (self.__reset - serverNow):spanseconds()
            timeout = timeout > 0 and math.max(5, timeout) or -1
            copas.sleep((self.__reset - serverNow):spanseconds())
        end
    end, o)

    return setmetatable(o, bucket_mt)
end

return bucket