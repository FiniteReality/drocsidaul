local util = { }

function util.merge(t, defaults)
    t = t or { }
    for i, v in pairs(defaults) do
        t[i] = t[i] or v
    end
    return t
end

return util