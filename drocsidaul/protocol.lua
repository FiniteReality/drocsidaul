local copas = require("copas")
local json = require("cjson")
local platform = require("drocsidaul.platform")
local gettime = require("socket").gettime

local protocol = { }

local opcode = {
    DISPATCH = 0,
    HEARTBEAT = 1,
    IDENTIFY = 2,
    PRESENCE = 3,
    VOICE_STATE = 4,
    VOICE_PING = 5,
    RESUME = 6,
    RECONNECT = 7,
    REQUEST_MEMBERS = 8,
    INVALIDATE_SESSION = 9,
    HELLO = 10,
    HEARTBEAT_ACK = 11,
    GUILD_SYNC = 12
}
for i, v in pairs(opcode) do
    opcode[v] = i
end
protocol.opcode = opcode

function protocol.send_heartbeat(client, timestamp)
    local diff =
        client.__cache.heartbeat_received - client.__cache.heartbeat_sent
    if diff > client.__cache.heartbeat_interval / 1000 then
        client:fire_event("log", "server missed last heartbeat, disconnecting")
        client:disconnect(1000, "heartbeat missed")
    else
        client:fire_event("log", "sending heartbeat")

        client.__cache.heartbeat_sent = timestamp
        client:send_opcode(opcode.HEARTBEAT, client.__cache.sequence)
    end
end

function protocol.send_identify(client)
    client:fire_event("log", "sending identify")
    client:send_opcode(opcode.IDENTIFY, {
        token = client.__token,
        properties = platform.gateway_properties,
        large_threshold = client.__options.large_threshold,
        shard = client.__cache.shard_info
    })
end

function protocol.handle_frame(client, payload)
    if payload.op == opcode.HELLO then
        client.__cache.heartbeat_interval = payload.d.heartbeat_interval

        client.__cache.heartbeat_thread = copas.addthread(function(self)
            client:fire_event("log", "heartbeat thread started")
            local heartbeat_interval = self.__cache.heartbeat_interval / 1000
            local last_heartbeat = gettime() - heartbeat_interval

            while self.state == "connected" do
                local now = gettime()

                -- only heartbeat if we need to
                if now - last_heartbeat >= heartbeat_interval then
                    protocol.send_heartbeat(self, now)
                    last_heartbeat = now
                end

                local waitTime = heartbeat_interval - (now - last_heartbeat)
                copas.sleep(waitTime)
            end
            client:fire_event("log",
                "client disconnected, heartbeat loop ending")
        end, client)

        protocol.send_identify(client)

    elseif payload.op == opcode.HEARTBEAT_ACK then
        if payload.d ~= json.null then
            if payload.d ~= client.__cache.sequence then
                client:fire_event("log",
                    ("received sequence (%s) did not match current, ignoring")
                        :format(payload.d))
            else
                client.__cache.received_sequence = payload.d
            end
        end

        local now = gettime()
        client.__cache.heartbeat_received = now
        client.latency = now - client.__cache.heartbeat_sent

        client:fire_event("latency", client.latency)

    elseif payload.op == opcode.DISPATCH then
        client:fire_event("dispatch", payload.t, payload.d)

        client.__cache.sequence = payload.s
    end

    copas.wakeup(client.__cache.heartbeat_thread)
end

return protocol