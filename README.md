# Drocsidaul #

Yet another Lua Discord library, because I'm bored.

## Dependencies ##

- LuaSocket
- LuaSec
- copas
- lua-websockets
- lua-cjson
- luadate
- lua-zlib
- uuid

## Running and testing ##

LuaFileSystem is needed for running the tests.

## TODO ##

- Add tests
- Add rockspec
- more code
- more support
- etc