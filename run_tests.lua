package.path = package.path .. ";./?/init.lua"

do
    -- some simple utility functions for quick-and dirty lua source parsing
    local old_assert = assert

    local function get_src(file)
        local f = old_assert(io.open(file))
        local res = old_assert(f:read("*a"))
        old_assert(f:close())
        return res
    end

    local function get_line(src, lineNo)
        src = src:gsub("\r\n?", "\n")
        local i = 0
        for line in src:gmatch("(.-)\n") do
            i = i + 1
            if i == lineNo then
                return line
            end
        end
    end

    -- replace assert so that we see exactly what failed
    function assert(expression, message)
        if not (expression) then
            local info = debug.getinfo(2, "Sl")
            if info.source:sub(1,1) == "@" then
                info.source = get_src(info.source:sub(2))
            end

            local line = get_line(info.source, info.currentline)
            line = line:match("assert%((.+)$"):match("^(.+),")

            error(("assertation '%s' failed: %s"):format(line, message), 2)
        end

        return expression
    end

    local copas = require("copas")

    -- use the debug api to override all unknown error handlers so that
    -- we print a stack trace for testing and exit afterwards
    local _, errHandlers = debug.getupvalue(copas.setErrorHandler, 1)
    setmetatable(errHandlers, {
        __index = function(self, i)
            return function(err, coro, socket)
                print(debug.traceback(coro, err))
                os.exit(1)
            end
        end
    })
end

dofile(...)