local copas = require("copas")
local drocsidaul = require("drocsidaul")

local token = assert(os.getenv("BOT_TOKEN"),
    "Pass 'BOT_TOKEN' as an environment variable!")

local function generate_ping_response(client)
    return ([[
**Hello, world!**
Message generated using %s!

**Version:** %s
**URL:** <%s>
**Copyright:** %s
**User Agent:** %s
**API version:** v%s (gateway v%s)
**Gateway latency:** %.2f ms
**Gateway encoding:** %s (using %s)]])
    :format(drocsidaul._NAME,
        drocsidaul._VERSION,
        drocsidaul._URL,
        drocsidaul._COPYRIGHT,
        client.__api.__options.user_agent:gsub("%((ht.-),", "(<%1>,"),
        client.__options.gateway_version, client.__api.__options.api_version,
        client.latency * 1000,
        client.__options.gateway_encoding,
        client.__options.gateway_compress or "no compression")
end

copas.addthread(function()
    local client = drocsidaul.client.new(token)
    _G._CLIENT = client

    client:on("connection_state", function(state)
        client:fire_event("log", ("client state changed to: %s"):format(state))
    end)

    local opcode = drocsidaul.protocol.opcode
    client:on("data", function(payload)
        if payload.t ~= "PRESENCE_UPDATE" then
            client:fire_event("log",
                ("received opcode %d (%s)"):format(payload.op or -1,
                    opcode[payload.op] or "unknown opcode"))
        end
    end)

    client:on("dispatch", function(name, data)
        if name ~= "PRESENCE_UPDATE" then
            client:fire_event("log", ("received dispatch: %s"):format(name))
        end

        if name == "MESSAGE_CREATE" then
            client:fire_event("log", ("received message from %s#%s (%s): %s")
                :format(data.author.username, data.author.discriminator,
                    data.author.id, data.content))

            if data.content == "drocsidaul pls ping" and
                not data.author.bot then

                client.__api:create_message(data.channel_id,
                    generate_ping_response(client))
            elseif data.content:find("^drocsidaul pls eval") and
                data.author.id == "81062087257755648" then

                local code = data.content:match("^drocsidaul pls eval (.+)$")
                local succ, err = loadstring(code)

                if not err then
                    local result = {pcall(succ)}
                    if not result[1] then
                        succ, err = result[1], result[2]
                    end

                    table.remove(result, 1)
                    succ, err = true, result
                end

                if type(err) == "table" then
                    for i, v in ipairs(err) do
                        err[i] = tostring(v)
                    end
                end

                if not succ then
                    client.__api:create_message(data.channel_id,
                        ("failed to execute:\n```\n%s\n```"):format(err))
                elseif #table.concat(err, '\n') > 1500 then
                    err.name = "result"
                    client.__api:create_message(data.channel_id,
                        "Script returned results which were too long to post.",
                        {
                            files = {
                                err
                            }
                        })
                elseif #err > 0 then
                    err = table.concat(err, '\n')
                    client.__api:create_message(data.channel_id,
                        ("Script returned:\n```\n%s\n```"):format(err))
                end
            end
        end
    end)


    client:on("latency", function(latency)
        client:fire_event("log", ("latency updated to: %.2f ms")
            :format(latency * 1000))
    end)

    client:on("log", function(msg)
        print(("[%s]"):format(os.date("!%c")), msg)
    end)

    while true do
        if not client:think() then
            client:connect()
        end
    end
end)

copas.loop()